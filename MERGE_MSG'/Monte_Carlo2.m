
function s = Monte_Carlo(f, x0, xk, n)
% Wyznacza przyblizenie calki funkcji f w granicach od x0 do xk
%   f   -   funkcja calkowana
%   x0  -   dolna granica calkowania
%   xk  -   gorna granica calkowania
%   n   -   ilosc powtorzen
%
%   

% Znalezienie maksimum z f:
x = x0:0.01:xk;
% Wyznaczenie zbioru calkowania
y = f(x);
figure();
plot(x,y,'b');
hold on
y_min = min(y);
y_max = max(y);
rect_area = (xk - x0) * (y_max - y_min); 
% Obliczanie pola prostokata

% Implementacja metody Monte Carlo:
score = 0;
for i = 1:n
    px = x0 + (xk - x0) * rand();
    py = y_min + (y_max - y_min) * rand();
    % Wyznaczanie wspolzednych punktow losowych 
    if f(px)>0 && py>0 && py<=f(px)
        score = score + 1;
        plot(px,py,'g.')
    elseif f(px)<0 && py<0 && py>=f(px)
        score = score - 1;
        plot(px,py,'g.')
        
    % Okreslanie w ktorej czesci funkcji lezy punkt 
    else
        plot(px,py,'r.')
    end
end
s = rect_area * score / n;
% Okreslanie przyblizonego wyniku wzorem wynikajacym z metody Monte Carlo
end    
    